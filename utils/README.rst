utilities
=========

This folder contains some utilities for *dramaTTS*:

text2wave.bat
-------------

MS Windows batch file to launch the text2wave script for festival.
If you have installed *festival* under "C:\\festival" you can just copy the to your local drive.
Otherwise you will need to adjust the file paths inside the file first.
