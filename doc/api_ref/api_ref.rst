API reference
=============

This chapter contains the API reference for the dramatts.core and dramatts.voices modules.

ScriptParser
------------

.. autoclass::  dramatts.core.ScriptParser
    :members:

AudioRenderer
-------------

.. autoclass::  dramatts.core.AudioRenderer
    :members:

ProjectManager
--------------

.. autoclass::  dramatts.core.ProjectManager
    :members:


Helper functions
----------------

.. autofunction::   dramatts.core.handle_getstatusoutput

.. autofunction::   dramatts.core.convert_version_string

VoiceConfig
-----------

.. autoclass::  dramatts.voices.VoiceConfig
    :members:

